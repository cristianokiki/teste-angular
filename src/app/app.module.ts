import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { PrimeiroTesteModule } from './primeiro-teste/primeiro-teste.module';
import { PrimeiroTesteComponent } from './primeiro-teste/primeiro-teste.component';
import { DispositivoModule } from './dispositivo/dispositivo.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    PrimeiroTesteModule,
    DispositivoModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
