import { Component, OnInit, Input } from '@angular/core';


import { PrimeiroTesteService } from './primeiro-teste.service';

@Component({
  selector: 'app-primeiro-teste',
  templateUrl: './primeiro-teste.component.html',
  styleUrls: ['./primeiro-teste.component.css']
})
export class PrimeiroTesteComponent implements OnInit {

  @Input("nome") cabecalho: string = '';

  urlImg = "http://lorempixel.com/400/200/";

  num: number;
  ativo: boolean;
  pessoas;
  nome: string = "kiki";
  tmNom;
  nomeImg: string = '';
  nomeCompleto: string = "";

  constructor(public primeiro: PrimeiroTesteService) {
    this.num = 0;
  }
 
  tamanhoNome(evento) {
    this.tmNom = (<HTMLInputElement>evento.target).value;
  }

  tamanhoNome2(evento) {
    this.tmNom = evento.value;
  }

  digitaNome() {
    this.num = this.num++;
  }

  mudaImagem(img) {
    return this.urlImg = img;
  }
  mudaCor() {
    return this.num % 2 == 0 ? "blue" : "red";
  }

  tamNome(nom: string) {

  }

  nomeCaixaAlta() {

    return this.nomeCompleto.charAt(this.nomeCompleto.length - 1).toUpperCase();
  }

  carregaLista() {

    if (this.ativo) {
      this.pessoas = this.primeiro.getPessoas();
    } else {
      this.pessoas = null;
    }
    this.isAtivo();
  }

  ngOnInit() {

  }

  aumenta() {
    this.num++;
  }

  diminui() {
    this.num--;
  }

  isAtivo() {
    this.ativo = !this.ativo;
  }

  testandoo() {
    alert("deu certo");
  }




}
