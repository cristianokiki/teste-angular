import { TestBed, inject } from '@angular/core/testing';

import { PrimeiroTesteService } from './primeiro-teste.service';

describe('PrimeiroTesteService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PrimeiroTesteService]
    });
  });

  it('should be created', inject([PrimeiroTesteService], (service: PrimeiroTesteService) => {
    expect(service).toBeTruthy();
  }));
});
