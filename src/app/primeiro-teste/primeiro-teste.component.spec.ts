import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PrimeiroTesteComponent } from './primeiro-teste.component';

describe('PrimeiroTesteComponent', () => {
  let component: PrimeiroTesteComponent;
  let fixture: ComponentFixture<PrimeiroTesteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrimeiroTesteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrimeiroTesteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
