import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PrimeiroTesteComponent } from './primeiro-teste.component';
import { PrimeiroTesteService } from './primeiro-teste.service';

import { FormsModule } from '@angular/forms';
import { EfeitoFotoComponent } from './efeito-foto/efeito-foto.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
  ],
  declarations: [PrimeiroTesteComponent,
  EfeitoFotoComponent],
  exports: [
    PrimeiroTesteComponent
  ],
  providers: [
    PrimeiroTesteService
  ]

})
export class PrimeiroTesteModule { }
