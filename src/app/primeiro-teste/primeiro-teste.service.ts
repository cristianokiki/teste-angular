import { Injectable } from '@angular/core';

@Injectable()
export class PrimeiroTesteService {

  constructor() { }

  getPessoas(): Pessoa[] {
    return [
      new Pessoa("cristiano", "Sobrenome", 22),
      new Pessoa("Homero", "Abreu", 34),
      new Pessoa("Maria", "Silvia", 60),
      new Pessoa("Lucas", "Santos", new Date().getTime())
    ];
  }
}

class Pessoa {

  constructor(
    public nome: string,
    public sobrenome: string,
    public idade: any
  ) { }


}
