import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EfeitoFotoComponent } from './efeito-foto.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [EfeitoFotoComponent],
  exports: [EfeitoFotoComponent]
})
export class EfeitoFotoModule { }
