import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EfeitoFotoComponent } from './efeito-foto.component';

describe('EfeitoFotoComponent', () => {
  let component: EfeitoFotoComponent;
  let fixture: ComponentFixture<EfeitoFotoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EfeitoFotoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EfeitoFotoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
