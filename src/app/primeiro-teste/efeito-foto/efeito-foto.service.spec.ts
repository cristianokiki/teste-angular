import { TestBed, inject } from '@angular/core/testing';

import { EfeitoFotoService } from './efeito-foto.service';

describe('EfeitoFotoService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EfeitoFotoService]
    });
  });

  it('should be created', inject([EfeitoFotoService], (service: EfeitoFotoService) => {
    expect(service).toBeTruthy();
  }));
});
