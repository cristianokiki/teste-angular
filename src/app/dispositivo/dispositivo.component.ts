import { Component, OnInit } from '@angular/core';
import { DispositivoService, Dispositivo } from './dispositivo.service';

@Component({
  selector: 'app-dispositivo',
  templateUrl: './dispositivo.component.html',
  styleUrls: ['./dispositivo.component.css']
})
export class DispositivoComponent implements OnInit {

  dispositivos: any = [];

  constructor(private dispositivo: DispositivoService) { }

  ngOnInit() {  
    console.log("iniciou");
    console.log(this.dispositivo.getDispositivos());  
   this.dispositivos = this.dispositivo.disp;
  }


}
