import { Injectable } from '@angular/core';

import 'rxjs/add/operator/toPromise';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';


@Injectable()
export class DispositivoService {

  private url: string = "http://localhost:8084/WebApplication1/webresources/dispositivo";

  public disp: Dispositivo[];

  constructor(private _http: Http) {
    this.getDispositivos();
  }


//tres metodos direfentes q tentei
  getDispositivos() {

   return  this._http.get(this.url).toPromise().then(response => this.disp = Array.of(response.json()));
  }

  getDispositivo(): Observable<Dispositivo> {
    return this._http.get(this.url)
      .map((response: Response) => <Dispositivo>response.json());
  }

  getDisp() {
    return this._http.get(this.url)
      .toPromise()
      .then(
      response => {
        this.disp = Array.of(response.json());
      });
  }

  retornaDispositivos() {

  }
}


export class Dispositivo {
  constructor(public idDispositivo: number,
    public descricao: string,
    public serialTerminal: string,
    public latitude: number,
    public longitude: number,
    public ultimoEnvio: Date) { }
}