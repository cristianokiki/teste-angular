import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DispositivoComponent } from './dispositivo.component';
import { DispositivoService } from './dispositivo.service';
import { HttpModule } from '@angular/http';

@NgModule({
  imports: [
    CommonModule,
    HttpModule
  ],
  declarations: [DispositivoComponent],
  exports: [DispositivoComponent],
  providers: [DispositivoService]
})
export class DispositivoModule { }
